//
//  LoginViewController.swift
//  DemoWatchTogether
//
//  Created by Артём Гуральник on 8/7/19.
//  Copyright © 2019 Артём Гуральник. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    private var token: String! {
        
        didSet {
            
            self.performSegue(withIdentifier: "authToLogin", sender: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if (UserDefaults.standard.value(forKey: "loginToken") as? String) != nil {
            
            self.performSegue(withIdentifier: "authToLogin", sender: nil)
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        guard let username = usernameTextField.text  else {
            return
        }
        
        
        guard let password = passwordTextField.text else {
            return
        }
        
        if password.count > 0, username.count > 0 {
            
            Alamofire.request("Authentication_server_login_url", method: .post, parameters: ["login": username, "password": password], encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                
                switch response.result {
                    
                case .success(let value):
                    
                    let json = JSON(value)
                    
                    guard let token = json["token"].string else {
                        
                        let alert = UIAlertController(title: nil, message:"Login error", preferredStyle: UIAlertController.Style.alert)
                        
                        let action1 = UIAlertAction(title: "Ok", style: .cancel)
                        
                        alert.addAction(action1)
                        
                        let closure = {[weak self] in
                            
                            self?.present(alert, animated: true, completion: nil)
                        }
                        
                        closure()
                        return
                    }
                    UserDefaults.standard.set(username, forKey: "username")
                    UserDefaults.standard.set(token, forKey: "loginToken")
                    
                    self.token = token

                    break
                case .failure(let error):
                    
                    let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                    
                    let action1 = UIAlertAction(title: "Ok", style: .cancel)
                    
                    alert.addAction(action1)
                    
                    let closure = {[weak self] in
                        
                        self?.present(alert, animated: true, completion: nil)
                    }
                    
                    closure()
                    
                    print(error.localizedDescription)
                    break
                }
                
            }
        }
    }
}

extension LoginViewController {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        if textField == usernameTextField {
            
            passwordTextField.becomeFirstResponder()
        }
        
        return true
    }
    
    override func touchesBegan(_: Set<UITouch>, with: UIEvent?) {
        
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as?  CredentialsViewController {
            
            if let usrName = UserDefaults.standard.value(forKey: "username") as? String {
                
                destination.userName = usrName
                
            }
        }
    }
}
