//
//  WebRTCUserCollectionViewCell.swift
//  DemoWatchTogether
//
//  Created by sceenic on 1/11/19.
//  Copyright © 2019 sceenic. All rights reserved.
//

import UIKit

protocol WebRTCUserCollectionViewCellDelegate {
    
    func enableAudio(onCell: WebRTCUserCollectionViewCell)
    func enableVideo(onCell: WebRTCUserCollectionViewCell)
    func switchCamera(onCell: WebRTCUserCollectionViewCell)
}

class WebRTCUserCollectionViewCell: UICollectionViewCell {
    
    var delegate: WebRTCUserCollectionViewCellDelegate?
    
    @IBOutlet weak var audioButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var webRTCStatsTextView: UITextView!
    @IBOutlet weak var webRTCStatsButton: UIButton!
    
    // MARK: - Initializer
    
    func displayRenderView(renderView: UIView) {
        
        renderView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(renderView)
        
        contentView.addConstraint(NSLayoutConstraint(item: contentView, attribute: .top, relatedBy: .equal, toItem: renderView, attribute: .top, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: renderView, attribute: .bottom, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: contentView, attribute: .leading, relatedBy: .equal, toItem: renderView, attribute: .leading, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: contentView, attribute: .trailing, relatedBy: .equal, toItem: renderView, attribute: .trailing, multiplier: 1, constant: 0))
        
        layoutIfNeeded()
        
        renderView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

        contentView.bringSubviewToFront(self.audioButton)
        contentView.bringSubviewToFront(self.videoButton)
        contentView.bringSubviewToFront(self.switchCameraButton)
        contentView.bringSubviewToFront(self.webRTCStatsTextView)
        contentView.bringSubviewToFront(self.webRTCStatsButton)
    }
    
    // MARK: - Actions
  
    @IBAction func volumeToggle(_ sender: Any) {
        delegate?.enableAudio(onCell: self)
    }
    
    @IBAction func videoToggle(_ sender: Any) {
        delegate?.enableVideo(onCell: self)
    }

    @IBAction func switchCameraToggle(_ sender: Any) {
        delegate?.switchCamera(onCell: self)
    }
    @IBAction func showRTCStats(_ sender: Any) {
        webRTCStatsTextView.isHidden = !webRTCStatsTextView.isHidden
    }
}
