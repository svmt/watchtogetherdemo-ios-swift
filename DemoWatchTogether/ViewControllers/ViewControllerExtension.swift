//
//  ViewControllerExtension.swift
//  DemoWatchTogether
//
//  Created by sceenic on 1/11/19.
//  Copyright © 2019 sceenic. All rights reserved.
//

import Foundation
import UIKit
import WatchTogetherLib

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCell", for: indexPath) as! WebRTCUserCollectionViewCell
        if indexPath.row == 0 {
            if let lcVideo = participantsArray[indexPath.row].getVideo() {
                  localRenderer = lcVideo
            }else {
                leaveAction(self)
            }
            cell.displayRenderView(renderView: localRenderer)
        }else {
            let participant = participantsArray[indexPath.row]
            cell.displayRenderView(renderView: participant.getVideo())
        }
        
        cell.delegate = self
        
        if indexPath.row != 0{
            cell.switchCameraButton.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let number = participantsArray else {
            return 0
        }
        return number.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: guestVideos.bounds.size.width/2.5, height: guestVideos.bounds.size.width/2)
    }
}

extension ViewController: WebRTCUserCollectionViewCellDelegate {
    
    func enableAudio(onCell: WebRTCUserCollectionViewCell) {
        guard let selectedIndexPath = guestVideos.indexPath(for: onCell) else {return}
        let participant = participantsArray[selectedIndexPath.row]
        if !participant.isAudioEnabled {
            participant.enableAudio { (enabled) in
                onCell.audioButton.setImage(UIImage(named:"ic_bottom_mic_on"), for: .normal)
            }
        }else {
            participant.disableAudio { (enabled) in
                onCell.audioButton.setImage(UIImage(named:"ic_bottom_mic_off"), for: .normal)
            }
        }
    }
    
    func enableVideo(onCell: WebRTCUserCollectionViewCell) {
        guard let selectedIndexPath = guestVideos.indexPath(for: onCell) else {return}
        let participant = participantsArray[selectedIndexPath.row]
        if !participant.isVideoEnabled {
            participant.enableVideo { (enabled) in
                onCell.videoButton.setImage(UIImage(named:"ic_bottom_video_on"), for: .normal)
            }
        }else {
            participant.disableVideo { (enabled) in
                onCell.videoButton.setImage(UIImage(named:"ic_bottom_video_off"), for: .normal)
            }
        }
    }
    
    func switchCamera(onCell: WebRTCUserCollectionViewCell) {
        if isFront{
            sessionManager?.switchCamera(to: AVCaptureDevice.Position.back)
        } else {
            sessionManager?.switchCamera(to: AVCaptureDevice.Position.front)
        }
        isFront = !isFront
        guestVideos.reloadItems(at: [IndexPath(row: 0, section: 0)])
        
        
        UIView.animate(withDuration: 0.2) {
            onCell.videoButton.setImage(UIImage(named:"ic_bottom_video_on"), for: .normal)
        }
    }
    
}

extension ViewController: SessionDelegate {
    func rtcStats(_ stats: [AnyHashable : Any]!, from participant: Participant!) {
        guard let index = participantsArray.index(of: participant) else {return}
        let indexPath = IndexPath(row: index, section: 0)
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: stats,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            DispatchQueue.main.async {[weak self] in
                guard let cell = self?.guestVideos.cellForItem(at: indexPath) as? WebRTCUserCollectionViewCell else {return}
                cell.webRTCStatsTextView.text += "\n\(theJSONText ?? "NONE")\n<------------>"
                let bottom = NSMakeRange(cell.webRTCStatsTextView.text.count - 1, 1)
                cell.webRTCStatsTextView.scrollRangeToVisible(bottom)
            }
        }
    }
    
    func session(onManageParticipantMedia participantID: String!, mediaType: MediaType, mediaState: MediaState) {
        let index = participantsArray.firstIndex { (participant) -> Bool in
            return participant.getID() == participantID
        }
        guard let row = index else {return}
        let indexPath = IndexPath(row: row, section: 0)
        guard let cell = guestVideos.cellForItem(at: indexPath) as? WebRTCUserCollectionViewCell else {return}
        switch mediaType {
        case AUDIO:
            switch mediaState {
            case ENABLED:
            cell.audioButton.setImage(UIImage(named:"ic_bottom_mic_on"), for: .normal)
            case DISABLED:
            cell.audioButton.setImage(UIImage(named:"ic_bottom_mic_off"), for: .normal)
            default:
                break
            }
            
        case VIDEO:
            switch mediaState {
            case ENABLED:
            cell.videoButton.setImage(UIImage(named:"ic_bottom_video_on"), for: .normal)
            case DISABLED:
            cell.videoButton.setImage(UIImage(named:"ic_bottom_video_off"), for: .normal)
            default:
                break
            }
            
        case ALL:
            switch mediaState {
            case ENABLED:
            cell.audioButton.setImage(UIImage(named:"ic_bottom_mic_on"), for: .normal)
            cell.videoButton.setImage(UIImage(named:"ic_bottom_video_on"), for: .normal)
            case DISABLED:
            cell.audioButton.setImage(UIImage(named:"ic_bottom_mic_off"), for: .normal)
            cell.videoButton.setImage(UIImage(named:"ic_bottom_video_off"), for: .normal)
            default:
                break
            }
            
        default:
            break
        }
    }
    
    func sessionDidGetStreamTime(_ time: String!, withUserNumber userNumber: Int) {
        guard let intTime = Int(time) else {return}
        secondsForSync = intTime
    }
    
    func sessionConnected(_ sessionId: String!, _ existPublisher: [String]!) {
        stackOfConnectionButtons.isHidden = true
        print("SessionID: \(sessionId)")
        print("Participants: \(existPublisher)")
    }
    
    func session(onLocalParticipantJoined participant: Participant!) {
        if participant != nil {
            participantsArray.append(participant)
            participant.startStats(withInterval: 5)// start handle stats
        }
    }
    
    func sessionDidFinish() {
        self.navigationController?.popViewController(animated: true)
        print("watchManagerDidFinish")
        participantsArray.removeAll()
    }
    
    func sessionParticipantJoined(_ participant: Participant!) {
        if participant != nil {
            participantsArray.append(participant)
            participant.startStats(withInterval: 5)// start handle stats
        }
    }
    
    func sessionParticipantLeft(_ participant: Participant!) {
        participantsArray.removeAll() { $0 == participant }
    }
  
    func sessionDidFailWithError(_ error: Error!) {
        print(error.localizedDescription)
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel){ _ in
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    func sessionOnOrganiserRecived() {
        print("You are organiser")
    }
}
