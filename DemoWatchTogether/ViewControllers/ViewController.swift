//
//  ViewController.swift
//  DemoWatchTogether
//
//  Created by sceenic on 1/10/19.
//  Copyright © 2019 sceenic. All rights reserved.
//

import UIKit
import WatchTogetherLib

protocol PopProtocol {
    
    func popViewController()
}

class ViewController: UIViewController {
    
    var delegate: PopProtocol?
    
    var isFront = true
    var displayName = ""
    var accessToken = ""
    
    var localRenderer: UIView!
    var participantsArray: [Participant]! {
        didSet {
            guestVideos.reloadData()
        }
    }

    var sessionManager: Session?
    var secondsForSync = 0
    var timerForSync: Timer?
    
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var stackOfConnectionButtons: UIStackView!
    @IBOutlet weak var fpsLable: UILabel!
    @IBOutlet weak var syncTimeLabel: UILabel!
    
    @IBOutlet weak var guestVideos: UICollectionView!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Session.enableStats()
        participantsArray = [Participant]()
        sessionManager = Session.build({ (builder) in
            builder?.delegate = self
            builder?.username = self.displayName
            builder?.accessToken = self.accessToken
        })
        sessionManager?.setupSession()
    }
    
    func startSyncTimer(){
        DispatchQueue.main.async {[weak self] in
            self?.timerForSync?.invalidate()
            self?.timerForSync = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
                guard let this = self else {return}
                this.secondsForSync+=1
                if this.secondsForSync % 3 == 0{
                    this.sessionManager?.sendPlayerData("\(this.secondsForSync)")
                }
                let seconds = this.secondsForSync % 60
                let minutes = this.secondsForSync / 60
                this.syncTimeLabel.text = "\(minutes):\(seconds)"
            })
        }
    }
    
    // MARK: - Actions
    
    @IBAction func leaveAction(_ sender: Any) {
        leaveRoom()
    }
    
    @IBAction func cameraButtonAction(_ sender: Any) {
        sessionManager?.startCameraPreview()
        cameraButton.isHidden = true
    }
    
    @IBAction func connectButtonAction(_ sender: Any) {
        sessionManager?.connect(withMode: .fullParticipant)
        startSyncTimer()
    }
    
    @IBAction func videoResolutionChanged(_ sender: UISegmentedControl) {
        var width = 0
        var height = 0
        
        switch sender.selectedSegmentIndex {
        case 0:
            width = 192;
            height = 144;
        case 1:
            width = 480;
            height = 360;
        case 2:
            width = 640;
            height = 480;
        default:
            width = 640;
            height = 480;
        }
        
        sessionManager?.adjustResolutionWidth(Int32(width), height: Int32(height))
        let indexPath = IndexPath(row: 0, section: 0)
        guestVideos.reloadItems(at: [indexPath])
    }
    
    @IBAction func adjustFrameRate(_ sender: UISlider) {
        let fps = Int32(sender.value)
        sessionManager?.adjustFrameRate(fps)
        fpsLable.text = "\(fps)"
    }
    
    // MARK: - Helpers
    
    func leaveRoom() {
        guard let session = sessionManager else {
            return
        }
        
        if session.isConnected() {
            session.disconnect()
            
            if localRenderer != nil {
                localRenderer = nil
            }
            sessionManager = nil
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
  
    func showError(error: String)  {
        let alert = UIAlertController(title: nil, message: error, preferredStyle: UIAlertController.Style.alert)
        let action1 = UIAlertAction(title: "Ok", style: .cancel, handler: { (_) in
            if (error == "Not authorized") {
                self.delegate?.popViewController()
            }else {
                self.dismiss(animated: true, completion: nil)
            }
        })
        alert.addAction(action1)
        let closure = {[weak self] in
            self?.present(alert, animated: true, completion: nil)
        }
        closure()
    }
}
