//
//  LoginViewController.swift
//  DemoWatchTogether
//
//  Created by sceenic on 8/7/19.
//  Copyright © 2019 sceenic. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var displayNameTextField: UITextField!
    
    private let accessToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginAction(_ sender: Any) {
        assert(accessToken.count > 0, "Streaming token is empty")
        
        guard let displayName = displayNameTextField.text else { return }
        self.goToStreamsViewController(displayName: displayName, accessToken: accessToken)
    }
    
    func goToStreamsViewController(displayName: String, accessToken: String) {
        guard let storyboard = self.storyboard else {return}
        guard let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ViewController.self)) as? ViewController else {return}
        vc.displayName = displayName
        vc.accessToken = accessToken
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension LoginViewController {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_: Set<UITouch>, with: UIEvent?) {
        view.endEditing(true)
    }
}
